#### The Command Line Tool! ####

Frequently, you have to copy your password into a CLI but pasting isn't always an option. Bitwarden makes this easy with their CLI client.

##### Installing the CLI #####
###### Linux ######
1. Download the Linux x64 executable from https://bitwarden.com/help/article/cli
2. unzip the bw-linux-x.x.x.zip file
 ```unzip bw-linux-x.x.x.zip ```
3. make the bw file executable
 ```sudo chmod +x bw```
4. Move the file to your bin directory
 ```sudo mv bw /usr/bin/bw```


###### Mac Homebrew ######
1. Run the command ```brew install bitwarden-cli```

##### Using the CLI #####

 To log in and sync your vault run the following command.

 ```sh
 bw login
 ```
 In order to make your session easier to use BW offers advice to run the following command:

 ```sh
 export BW_SESSION="xxxxxxxxxxxxx=="
 ```

 Now you can sync your vault with:
```sh
bw sync
```

##### Available Commands #####
There are lots of available commands. Visit https://bitwarden.com/help/article/cli for all of the available commands.

[Next Page](finalnote.md)
