#### Installation of Bitwarden Clients ####

Install local client:

1. Navigate to https://bitwarden.com/download
2. Download the client for your Operating System
3. Follow instructions to install
4. Launch and Log In

Install Web Browser Extension:

This will vary by web browser. instructions can be found for each browser at https://bitwarden.com/download

Install on your Phone:

This will vary by OS. instructions can be found for each browser at https://bitwarden.com/download


[Next Page](importing.md)
