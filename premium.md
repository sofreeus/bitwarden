#### Why is it worth upgrading to Premium ####

1. 1 GB encrypted storage for file attachments.
2. 2 Step login options.
3. Password reporting (Warning of passwords found in breaches)
4. TOTP Verification Tool (Google Authenticator Alternative)
5. $10 a year and supports an Open Source Project

[Next Page](commandline.md)
