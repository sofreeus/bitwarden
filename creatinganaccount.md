#### Creating an Account ####

To create an account:

1. Navigate to https://bitwarden.com
2. Click on "Get Started" on top right of screen.
![Alt Text](./getstarted.png "Get Started")


3. Fill in the information in the Create Account Screen.
![Alt Text](./signup.png "Sign Up")


##### Important Note! #####
The Master Password that you select at this point cannot be changed. Only paid accounts are allowed to change the master password so make it a good one.

4. Log In
![Alt Text](./loginscreen.png "Log In")

5. Verify your email.
  * Click the 'Send Email' button in your vault.
  * Log into your email and click the link to verify your email.

You have now created your account for Bitwarden. It's time to install the client.

[Next Page](installation.md)
