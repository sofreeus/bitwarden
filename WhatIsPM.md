Welcome to the Introduction to Bitwarden.

Who Am I: Gary Romero
          Systems Administrator, Level II. Regis University


#### What is a password manager?
###### From Wikipedia

A password manager is a computer program that allows users to store, generate, and manage their passwords for local applications and online services.

A password manager assists in generating and retrieving complex passwords, storing such passwords in an encrypted database[1][2] or calculating them on demand.[3]

Types of password managers include:

* locally installed software applications
* online services accessed through website portals
* locally accessed hardware devices that serve as keys

Depending on the type of password manager used and on the functionality offered by its developers, the encrypted database is either stored locally on the user's device or stored remotely through an online file-hosting service. Password managers typically require a user to generate and remember one "master" password to unlock and access any information stored in their databases. Many password manager applications offer additional capabilities that enhance both convenience and security such as storage of credit card and frequent flyer information and autofill functionality.

[Next Page](welcometobitwarden.md)
