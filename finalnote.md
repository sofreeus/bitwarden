#### Hosting your own sync server! ####

It is important to note that you can host your own sync server. If you are like many other people in the Open Source community, you would much rather keep your information on your own servers. Bitwarden allows for this in both the free and premium versions.

If you interested in learning how, stick around. I'll be working to set up a sync server on the SFS ProxMox server during the hacking hours following this class.

# Thank you #
