#### Importing your previous PM's data ####

Bitwarden makes migrations incredibly easy to do.

1. Log into your vault online at https://vault.bitwarden.com
2. Click on Tools > Import Data
3. Select the format of the import file
4. Select the file to be imported
5. Click Import data
  * If you're using KeePass 2:
    1. Log intot he desktop app
    2. Navigate to file -> export
    3. Select the "KeePass XML(2.x)" option
  * For all others, follow online instruction

[Next Page](premium.md)
