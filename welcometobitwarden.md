#### Welcome to Bitwarden

www.bitwarden.com

Bitwarden is an Open Source Password Manager that includes central account syncing with End-to-End AES-256 bit encryption.

Why is Bitwarden better than other Password Managers?

 Provider | Price | Import from browsers | Import from competitors | Multi-factor authentication | Export data | Automatic password capture | Automatic password replay | Forms | Multiple form- filling identities | Central Syncing | Actionable password strength report | Secure sharing | Digital_inheritance|Digital legacy | Portable edition | Application passwords | Browser menu of logins | Application-level encryption | Secure Password Sharing
--- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- |
1Password | $3–5 (monthly) | Yes | Yes | Yes | Yes | Yes | Yes | Yes | Yes | Yes | Yes | Yes | Yes | Yes | Yes | Yes | Yes | Yes
Bitwarden | Free/Open Source or $10 (yearly) | Yes | Yes | Yes | Yes | Yes | Yes | Yes | Yes | Yes | Yes | Yes | depends | Yes Read only | Yes | Yes | Yes | Yes
KeePass | Free/Open Source | Yes | Yes | Yes (Plugin) | Yes | Yes | Yes | Yes | Yes | No | Yes | Optional (Requires Pleasant Password Server|add-on server) | depends | Yes | Yes | Yes | Yes | ?
KeePassXC | Free/Open Source | Yes | Yes | Yes (Plugin) | Yes | Yes | Yes | Yes | Yes | No | Yes | No | depends | Yes | Yes | Yes | Yes | ?
LastPass | Free or $36.00 (yearly) | Yes | Yes | Yes | Yes | Yes | Yes | Yes | Yes | Yes | Yes | Yes | Yes | Yes | Yes | Yes | Yes | Yes

* Bitwarden is Open Source.
* Premium is only $10 a year.
* You can host your own syncing server.
* All OS's supported!

[Next Page](creatinganaccount.md)
